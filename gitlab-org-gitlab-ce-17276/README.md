## Check for gitlab-org/gitlab-ce#17276

### SVG in the repo (relative URL)

![](logo.svg)

via https://gitlab.com/gitlab-org/gitlab-ce

### PNG in the repo (relative URL)

![](icon-search.png)

via https://gitlab.com/gitlab-org/gitlab-ce

### SVG with full URL

![](https://upload.wikimedia.org/wikipedia/commons/0/02/SVG_logo.svg)

via https://commons.wikimedia.org/wiki/File:SVG_logo.svg

### PNG with full URL

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/SVG_logo.svg/240px-SVG_logo.svg.png)

via https://commons.wikimedia.org/wiki/File:SVG_logo.svg
